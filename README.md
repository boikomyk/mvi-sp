# Video Resolution Upscaling Using Neural Networks

* Take a short 5-10 second video and create a generator that will generate higher resolution video.
* Use both architectures: GAN and U-Net.
* Compare the results.

## First iteration
* [Research](https://docs.google.com/document/d/1GF3xds-HbAU14UTalFxd9k7y3fG6SMTgNJVBW5rV_8s/edit?usp=sharing)
* [Google colab file](https://colab.research.google.com/drive/1W4heOnUY8BV0JfpCgakXobIjSCSihaSc?usp=sharing)

## Upscaling
The main objective is create tool/generator that will generate higher resolution videos.
This project uses functionality from this github repo [Image Super-Resolution (ISR)](https://github.com/idealo/image-super-resolution). It provides several useful models for our purposes. But, mainly, we are interested in
- GAN version of image upscaling called ESRGAN
- Residual Dense Network for Image Super-Resolution

## Installation
This project has following dependencies:
 * python3.6

and related python libraries, you can easily install them by executing:

``` pip3.6 install ISR opencv-python matplotlib Pillow numpy click tqdm  ```

## Project tools (cmds in semestral_work/):
 - [video_downscaler.py](https://gitlab.fit.cvut.cz/boikomyk/mvi-sp/blob/master/semestral_work/video_downscaler.py) - Downscale input video 4 times
 - [video_upscaler.py](https://gitlab.fit.cvut.cz/boikomyk/mvi-sp/blob/master/semestral_work/video_upscaler.py) - Upscale input video 4 times with RRDN network
 - [video_splitter.py](https://gitlab.fit.cvut.cz/boikomyk/mvi-sp/blob/master/semestral_work/video_splitter.py) - Split input video to frames
 - [frames_to_video_convertor.py](https://gitlab.fit.cvut.cz/boikomyk/mvi-sp/blob/master/semestral_work/frames_to_video_convertor.py) - Union frames from input directory to a single video

## Experiments:
 - [First experiment using all 4 networks and interpolation methods](https://gitlab.fit.cvut.cz/boikomyk/mvi-sp/blob/master/milestone_research/milestone.ipynb)
 - [Second final experiment with 2 best networks and interpolation methods in video upscaling](https://gitlab.fit.cvut.cz/boikomyk/mvi-sp/blob/master/semestral_work/experiment_with_video.ipynb)

## Project sources (files in semestral_work/src/*):
 - image_wrapper.py - contains ImageWrapper class
 - model.py - contains trained networks
 - video_editor.py - contains class/tool for video write/read operations 
 
## License
[MIT](https://choosealicense.com/licenses/mit/)

* [Mykyta Boiko](https://github.com/boikomyk)